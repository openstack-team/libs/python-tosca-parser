python-tosca-parser (2.12.0-1) experimental; urgency=medium

  * New upstream release.
  * d/watch: switch to version=4 and mode=git.
  * Rebased remove-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Wed, 26 Feb 2025 14:14:01 +0100

python-tosca-parser (2.11.0-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090628).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 22:08:34 +0100

python-tosca-parser (2.11.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 19 Sep 2024 17:37:08 +0200

python-tosca-parser (2.11.0-1) experimental; urgency=medium

  * New upstream release.
  * Setup export RES_OPTIONS=attempts:0 in d/rules, removed patch
    remove-network-access-unit-tests.patch and blacklist failed unit tests.
  * Add remove-test.patch, as toscaparser/tests/test_tosca_nfv_tpl.py is
    causing test discovery failure.
  * Blacklist unit tests doing network access:
    - test_prereq.CSARPrereqTest.test_csar_invalid_import_url
    - test_toscatpl.ToscaTemplateTest.test_repositories
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Mon, 26 Aug 2024 12:42:16 +0200

python-tosca-parser (2.10.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2024 11:31:08 +0100

python-tosca-parser (2.9.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 22:57:24 +0200

python-tosca-parser (2.9.1-1) experimental; urgency=medium

  * New upstream release.
  * Rebased remove-network-access-unit-tests.patch.
  * Blacklist 2 unit tests doing network access.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Aug 2023 11:48:34 +0200

python-tosca-parser (2.8.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1048215).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 17:34:57 +0200

python-tosca-parser (2.8.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Jun 2023 10:58:11 +0200

python-tosca-parser (2.8.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 27 Feb 2023 09:39:12 +0100

python-tosca-parser (2.6.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 23 Sep 2022 12:15:45 +0200

python-tosca-parser (2.6.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 29 Aug 2022 15:24:37 +0200

python-tosca-parser (2.5.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 11:02:29 +0100

python-tosca-parser (2.5.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Feb 2022 13:47:53 +0100

python-tosca-parser (2.5.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Thu, 17 Feb 2022 14:50:54 +0100

python-tosca-parser (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 29 Sep 2021 12:12:15 +0200

python-tosca-parser (2.4.1-1) experimental; urgency=medium

  [ Mickael Asseline ]
  * New upstream release.
  * Added myself in copyright and uploaders.

 -- Thomas Goirand <zigo@debian.org>  Tue, 24 Aug 2021 11:51:02 +0200

python-tosca-parser (2.3.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 19:16:54 +0200

python-tosca-parser (2.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 13:19:09 +0100

python-tosca-parser (2.1.1-3) unstable; urgency=medium

  * Added Restrictions: superficial to d/tests/control (Closes: #974520).

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Nov 2020 23:56:42 +0100

python-tosca-parser (2.1.1-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 14:33:24 +0200

python-tosca-parser (2.1.1-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 22:00:02 +0200

python-tosca-parser (2.1.0-1) experimental; urgency=medium

  * New uptream release.
  * Refresh patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Sep 2020 08:33:07 +0200

python-tosca-parser (2.0.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 12:01:29 +0200

python-tosca-parser (2.0.0-1) experimental; urgency=medium

  [ Thomas Goirand ]
  * New upstream release.
  * Bump Standards-Version to 4.5.0.
  * debhelper-compat bump to 11.

 -- Thomas Goirand <zigo@debian.org>  Tue, 07 Apr 2020 09:27:55 +0200

python-tosca-parser (1.4.0-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast.
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Thomas Goirand ]
  * Drop python2 test, thanks to Gianfranco Costamagna locutusofborg at
    debian.org (Closes: #933345).

 -- Thomas Goirand <zigo@debian.org>  Tue, 30 Jul 2019 23:22:28 +0200

python-tosca-parser (1.4.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 17 Jul 2019 00:42:47 +0200

python-tosca-parser (1.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Removed Python 2 support.
  * Blacklist failing test: ToscaTemplateTest.test_multiple_policies().

 -- Thomas Goirand <zigo@debian.org>  Wed, 27 Mar 2019 15:04:23 +0100

python-tosca-parser (1.1.0-3) unstable; urgency=medium

  * override_dh_python3: dh_python3 --shebang=/usr/bin/python3.

 -- Thomas Goirand <zigo@debian.org>  Fri, 28 Dec 2018 16:44:23 +0100

python-tosca-parser (1.1.0-2) unstable; urgency=medium

  * Disable networking and resolving when running unit tests.
  * Patch out 16 unit tests doing network access (Closes: #908391).

 -- Thomas Goirand <zigo@debian.org>  Mon, 10 Sep 2018 00:01:37 +0200

python-tosca-parser (1.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * New upstream release.
  * Fixed (build-)depends for new release.
  * Added myself to d/copyright.
  * Fixed order in d/copyright.
  * Install /usr/bin/tosca-parser as alternatives in both packages.
  * Added Debian tests.
  * Added upstream changelog.
  * Fixed hardcoded Python 2.7 version when running unit tests
  * Standards-Version is 3.9.8 now (no change)
  * d/rules: Changed UPSTREAM_GIT protocol to https
  * d/copyright: Changed source URL to https protocol
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Use team+openstack@tracker.debian.org as maintainer
  * d/control: Set Vcs-* to salsa.debian.org

  [ Daniel Baumann ]
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release:
    - Fix FTBFS (Closes: #907966).
  * Fixed (build-)depends for this release.
  * Using python3 -m sphinx to build the doc.
  * Remove obsolete stuff from debian/rules and use the new
    pkgos-dh_auto_{install,test} scripts.

 -- Thomas Goirand <zigo@debian.org>  Fri, 07 Sep 2018 13:51:24 +0200

python-tosca-parser (0.1.0-3) unstable; urgency=medium

  * Added missing openstack-pkg-tools build-depends (Closes: #802150).

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Oct 2015 14:11:41 +0000

python-tosca-parser (0.1.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 12:22:51 +0000

python-tosca-parser (0.1.0-1) experimental; urgency=medium

  * Initial release. (Closes: #797775)

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Sep 2015 14:06:14 +0200
